
Employee roster application
This repository contains a simple task management app built with React and Redux.

Installation
Clone this repository to your local machine:

git clone <https://gitlab.com/krprasad32/employee-roster/-/tree/main/code/employee-information?ref_type=heads>

Navigate to the project directory:

cd code

Install dependencies:

npm install


Running the App
To start the development server, run:

npm start
Open http://localhost:3000 to view it in the browser.

Running Tests
To run tests, use the following command:


npm run test
Building the App
To build the app for production, run:

npm run build


Folder Structure

actions: Contains Redux action files.
components: Holds UI components and their test cases.
reducer: Contains Redux reducer files.
service: Includes Saga files with asynchronous code and API calls.
store: Contains Redux store setup with middleware for Saga.


Additional Notes
Sample data JSON files should be moved to the public folder.
The App.js file is located in the root folder.