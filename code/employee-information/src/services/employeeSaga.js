import { put, takeLatest } from 'redux-saga/effects';
import { FETCH_EMPLOYEES_REQUEST, fetchEmployeesSuccess } from '../actions/employeeActions';

function* fetchEmployeesSaga() {
    try {
        const response = yield fetch('/sample-data-employees.json');
        if (!response.ok) {
            throw new Error('Failed to fetch employees');
        }
        const data = yield response.json();
        yield put(fetchEmployeesSuccess(data));
    } catch (error) {
        console.error('Error fetching employees:', error);
    }
}

function* rootSaga() {
    yield takeLatest(FETCH_EMPLOYEES_REQUEST, fetchEmployeesSaga);
}

export default rootSaga;