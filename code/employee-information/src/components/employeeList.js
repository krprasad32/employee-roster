import React from "react";
import { FaSortUp, FaSortDown } from "react-icons/fa";
import EmployeeDetailModal from "./employee-detail-modal";
import "./employeelist.css";

const EmployeeList = ({ employees }) => {
  const [selectedEmployee, setSelectedEmployee] = React.useState(null);
  const [modalVisible, setModalVisible] = React.useState(false);
  const [searchTerm, setSearchTerm] = React.useState("");
  const [currentPage, setCurrentPage] = React.useState(1);
  const [sortBy, setSortBy] = React.useState("");
  const [sortOrder, setSortOrder] = React.useState("");
  const [rowsPerPage] = React.useState(5);

  const indexOfLastRow = currentPage * rowsPerPage;
  const indexOfFirstRow = indexOfLastRow - rowsPerPage;

  const currentRows = employees?.slice(indexOfFirstRow, indexOfLastRow);

  const openModal = (employee) => {
    setSelectedEmployee(employee);
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
  };

  const handleSearch = (event) => {
    setSearchTerm(event.target.value);
  };

  // Sorting logic
  const sortedRows = currentRows.sort((a, b) => {
    if (sortOrder === "asc") {
      return a[sortBy].localeCompare(b[sortBy]);
    } else if (sortOrder === "desc") {
      return b[sortBy].localeCompare(a[sortBy]);
    } else {
      return 0;
    }
  });

  const filteredRows = sortedRows?.filter(
    (row) =>
      row.firstName.toLowerCase().includes(searchTerm.toLowerCase()) ||
      row.lastName.toLowerCase().includes(searchTerm.toLowerCase()) ||
      row.contactNo.includes(searchTerm)
  );

  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const handleSort = (field) => {
    if (field === sortBy) {
      setSortOrder(sortOrder === "asc" ? "desc" : "asc");
    } else {
      setSortBy(field);
      setSortOrder("asc");
    }
  };

  return (
    <div className="table-wrapper">
      <div className="filter-container">
        <input
          type="text"
          placeholder="Search by name"
          className="search-input"
          onChange={handleSearch}
        />
        {/* <button className="search-button">Search</button> */}
      </div>
      <div>
        <span>{`Showing ${indexOfFirstRow + 1}-${
          indexOfLastRow > employees.length ? employees.length : indexOfLastRow
        } out of ${employees.length}`}</span>
      </div>
      <table className="responsive-table">
        <thead>
          <tr>
            <th>Id</th>
            <th onClick={() => handleSort("firstName")}>
              Name
              {sortBy === "firstName" &&
                (sortOrder === "asc" ? <FaSortUp /> : <FaSortDown />)}
            </th>
            <th onClick={() => handleSort("contactNo")}>
              Phone Number
              {sortBy === "contactNo" &&
                (sortOrder === "asc" ? <FaSortUp /> : <FaSortDown />)}
            </th>
            <th onClick={() => handleSort("address")}>
              Address
              {sortBy === "address" &&
                (sortOrder === "asc" ? <FaSortUp /> : <FaSortDown />)}
            </th>
          </tr>
        </thead>
        <tbody>
          {filteredRows?.map((employee) => (
            <tr key={employee.id} onClick={() => openModal(employee)}>
              <td>{employee.id}</td>
              <td>
                <div className="name-container">
                  <img
                    src={employee.avatar}
                    alt={employee.firstName}
                    className="avatar"
                  />
                  <div>
                    {employee.firstName} {employee.lastName}
                  </div>
                </div>
              </td>
              <td>{employee.contactNo}</td>
              <td>{employee.address}</td>
            </tr>
          ))}
        </tbody>
      </table>
      {modalVisible && (
        <EmployeeDetailModal
          employee={selectedEmployee}
          closeModal={closeModal}
        />
      )}
      <div className="pagination">
        <button
          onClick={() => paginate(currentPage - 1)}
          disabled={currentPage === 1}
        >
          Previous
        </button>
        <button
          onClick={() => paginate(currentPage + 1)}
          disabled={indexOfLastRow >= employees.length}
        >
          Next
        </button>
      </div>
    </div>
  );
};

export default EmployeeList;
