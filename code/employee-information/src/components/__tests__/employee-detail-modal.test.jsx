import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import EmployeeDetailModal from "../employee-detail-modal";

describe("EmployeeDetailModal", () => {
  const employee = {
    id: "832f393b-d669-4939-adfc-6b4ac6c40931",
    avatar: "https://s3.amazonaws.com/uifaces/faces/twitter/omnizya/128.jpg",
    firstName: "Ella",
    lastName: "Mccullough",
    jobTitle: "Software Engineer",
    contactNo: "0496 793 477",
    address: "Johnson Plaza Madelinefort, South Australia",
    age: 42,
    bio: "Reprehenderit eum aut quo. Qui iusto sequi iure voluptas provident enim quisquam ea. Accusantium accusantium sit ut. Dolor nostrum modi reiciendis. Quia ut autem dolorem ut. Laudantium maiores impedit est magnam.",
    dateJoined: "2021-04-10T02:49:37.654Z",
  };

  it("should renders employee details correctly", () => {
    render(<EmployeeDetailModal employee={employee} closeModal={() => {}} />);
    expect(screen.getByText("Software Engineer")).toBeInTheDocument();
  });

  it("calls closeModal when close button is clicked", () => {
    const closeModalMock = jest.fn();
    render(
      <EmployeeDetailModal employee={employee} closeModal={closeModalMock} />
    );
    // const closeButton = screen.getByText("Close");
    const closeButton = screen.getByTestId("modal-close-button");

    fireEvent.click(closeButton);

    expect(closeModalMock).toHaveBeenCalled();
  });
});