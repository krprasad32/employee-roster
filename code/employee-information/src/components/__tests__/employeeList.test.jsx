import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import EmployeeList from '../employeeList';

describe('EmployeeList', () => {
    const employees = [
        { id: 1, firstName: 'John', lastName: 'Doe', contactNo: '1234567890', address: '123 Main St' },
        { id: 2, firstName: 'Jane', lastName: 'Doe', contactNo: '9876543210', address: '456 Elm St' }
    ];

    it('should renders employee list correctly', () => {
        render(<EmployeeList employees={employees} />);
        
        expect(screen.getByText('John Doe')).toBeInTheDocument();
        expect(screen.getByText('Jane Doe')).toBeInTheDocument();
        expect(screen.getByText('1234567890')).toBeInTheDocument();
        expect(screen.getByText('9876543210')).toBeInTheDocument();
        expect(screen.getByText('123 Main St')).toBeInTheDocument();
        expect(screen.getByText('456 Elm St')).toBeInTheDocument();
    });

    it('should allows searching by name', () => {
        render(<EmployeeList employees={employees} />);
        const searchInput = screen.getByPlaceholderText('Search by name');

        fireEvent.change(searchInput, { target: { value: 'John' } });

        expect(screen.getByText('John Doe')).toBeInTheDocument();
        expect(screen.queryByText('Jane Doe')).not.toBeInTheDocument();
    });

    it('should allows sorting by name in ascending order', () => {
        render(<EmployeeList employees={employees} />);
        const nameHeader = screen.getByText('Name');

        fireEvent.click(nameHeader);

        expect(screen.getByText('Jane Doe')).toBeInTheDocument();
        expect(screen.getByText('John Doe')).toBeInTheDocument();
    });

    it('should allows sorting by name in descending order', () => {
        render(<EmployeeList employees={employees} />);
        const nameHeader = screen.getByText('Name');

        fireEvent.click(nameHeader); 
        fireEvent.click(nameHeader);

        expect(screen.getByText('Jane Doe')).toBeInTheDocument();
        expect(screen.getByText('John Doe')).toBeInTheDocument();
    });

});
