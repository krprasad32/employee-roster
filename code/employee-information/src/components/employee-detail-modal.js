import React, { useRef, useEffect } from 'react';
import { IoCloseSharp } from "react-icons/io5";
import './employee-modal.css';

const EmployeeDetailModal = ({ employee, closeModal }) => {
    const modalRef = useRef(null);

    useEffect(() => {
        const handleOutsideClick = (event) => {
            if (modalRef.current && !modalRef.current.contains(event.target)) {
                closeModal();
            }
        };

        document.addEventListener("mousedown", handleOutsideClick);

        return () => {
            document.removeEventListener("mousedown", handleOutsideClick);
        };
    }, [closeModal]);

    return (
        <div className="modal-overlay">
            <div className="modal" ref={modalRef}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h2>Employee Details</h2>
                        <button data-testid="modal-close-button" className="close-button" onClick={closeModal}><IoCloseSharp /></button>
                    </div>
                    <div className="modal-body">
                        <div className="image-container">
                            <img src={employee.avatar} alt={employee.firstName} className="employee-avatar" />
                        </div>
                        <div className="details-container">
                            <p><strong>Name:</strong> {employee.firstName} {employee.lastName}</p>
                            <p><strong>Contact No:</strong> {employee.contactNo}</p>
                            <p><strong>Address:</strong> {employee.address}</p>
                            <p><strong>Job Title:</strong> {employee.jobTitle}</p>
                            <p><strong>Age:</strong> {employee.age}</p>
                            <p><strong>Bio:</strong> {employee.bio}</p>
                            <p><strong>Date Joined:</strong> {employee.dateJoined}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default EmployeeDetailModal;