import React from 'react';

const CompanyInfo = ({companyInfo}) => {
    return (
        <div className="company-info">
            <h2>Company Information</h2>
            <div className="info-item">
                <span className="label">Company Name:</span>
                <span className="value">{companyInfo?.companyName}</span>
            </div>
            <div className="info-item">
                <span className="label">Company Motto:</span>
                <span className="value">{companyInfo?.companyMotto}</span>
            </div>
            <div className="info-item">
                <span className="label">Established Date:</span>
                <span className="value">{new Date(companyInfo?.companyEst)?.toDateString()}</span>
            </div>
        </div>
    );
};

export default CompanyInfo;