import { FETCH_EMPLOYEES_SUCCESS } from '../actions/employeeActions';

const initialState = {
    employees: [],
    companyDetails: {}
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_EMPLOYEES_SUCCESS:
            return {
                ...state,
                employees: action?.payload?.employees,
                companyDetails: action?.payload?.companyInfo
            };
        default:
            return state;
    }
};

export default reducer;