import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { fetchEmployeesRequest } from "./actions/employeeActions";
import EmployeeList from "./components/employeeList";
import CompanyInfo from "./components/companyInfo";

import "./App.css";

const App = () => {
  const dispatch = useDispatch();
  const { employees, companyDetails } = useSelector((state) => state);


  useEffect(() => {
    dispatch(fetchEmployeesRequest());
  }, [dispatch]);

  return (
    <div className="App">
      <CompanyInfo companyInfo={companyDetails} />
      <div className="list-container">
        <EmployeeList employees={employees} />
      </div>
    </div>
  );
};

export default App;
