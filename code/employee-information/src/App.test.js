import { render } from "@testing-library/react";
import { useDispatch, useSelector } from "react-redux";
import App from "./App";

jest.mock("react-redux", () => ({
  ...jest.requireActual("react-redux"),
  useDispatch: jest.fn(),
  useSelector: jest.fn(),
}));

const fetchEmployeesRequestMock = jest.fn();

const mockState = {
  employees: [
    {
      id: "832f393b-d669-4939-adfc-6b4ac6c40931",
      avatar: "https://s3.amazonaws.com/uifaces/faces/twitter/omnizya/128.jpg",
      firstName: "Ella",
      lastName: "Mccullough",
      jobTitle: "Direct Functionality Administrator",
      contactNo: "0496 793 477",
      address: "Johnson Plaza Madelinefort, South Australia",
      age: 42,
      bio: "Reprehenderit eum aut quo. Qui iusto sequi iure voluptas provident enim quisquam ea. Accusantium accusantium sit ut. Dolor nostrum modi reiciendis. Quia ut autem dolorem ut. Laudantium maiores impedit est magnam.",
      dateJoined: "2021-04-10T02:49:37.654Z",
    },
    {
      id: "73bc19ec-7ab0-4dec-973e-b4ee96c011f3",
      avatar:
        "https://s3.amazonaws.com/uifaces/faces/twitter/reideiredale/128.jpg",
      firstName: "Harrison",
      lastName: "Swift",
      jobTitle: "Dynamic Branding Specialist",
      contactNo: "0434 329 199",
      address: "Tahlia Street South Michael, Australian Capital Territory",
      age: 59,
      bio: "Aut qui eaque fuga ullam maiores autem eligendi. Consequuntur optio vitae provident distinctio dolores voluptate deleniti. Labore ipsam et consequatur non iusto quos et inventore. Vel deleniti ea nam. Eum iste unde ipsa molestiae qui. Omnis dolores et aspernatur eius eaque.",
      dateJoined: "2021-12-10T04:43:47.173Z",
    },
  ],
  companyDetails: {
    companyName: "Example Company",
    companyMotto: "Lorem Ipsum",
    companyEst: "2022-01-01",
  },
};
useSelector.mockImplementation((selectorFn) => selectorFn(mockState));

describe("App", () => {
  it("fetchEmployeesRequest is called on mount", () => {
    useSelector.mockReturnValue({
      ...mockState,
    });
    useDispatch.mockReturnValue(fetchEmployeesRequestMock);
    render(<App />);
    expect(fetchEmployeesRequestMock).toHaveBeenCalled();
  });
});
